/**
 * Created by Daniel on 4/7/2017.
 */
public class vectorEntry {
    public static void main(String[] args) {
        vector a = new vector(1, 2, 3);
        vector b = new vector(5, 6, 7);
        System.out.print("Vector A: ");
        a.print();
        System.out.print("Vector B: ");
        b.print();
        System.out.println("A dot B: " + a.dot(b));
        System.out.print("A cross B: ");
        a.cross(b).print();
    }
}

class vector {
    private int i;
    private int j;
    private int k;

    public vector(int i, int j, int k) {
        this.i = i;
        this.j = j;
        this.k = k;
    }

    public void print() {
        System.out.println("<" + this.i + ", " + this.j + ", " + this.k + ">");
    }

    public vector add(vector other) {
        int x = this.i + other.i;
        int y = this.j + other.j;
        int z = this.k + other.k;
        return new vector(x, y, z);
    }

    public vector subtract(vector other) {
        int x = this.i - other.i;
        int y = this.j - other.j;
        int z = this.k - other.k;
        return new vector(x, y, z);
    }

    public int dot(vector other) {
        return this.i * other.i + this.j * other.j + this.k * other.k;
    }

    public double magnitude() {
        return magnitude(this.i, this.j, this.k);
    }

    private double magnitude(int x, int y, int z) {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public vector cross(vector other) {
        int x = (this.j * other.k - other.j * this.k);
        int y = (this.i * other.k - other.i * this.k);
        int z = (this.i * other.j - other.i * this.j);
        return new vector(x, y, z);
    }
}
